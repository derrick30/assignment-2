let map; 
 
// This should be your own API key 
mapboxgl.accessToken = 'pk.eyJ1Ijoia2toZXIyOCIsImEiOiJja3g1d2JscmYxY2tvMnVwbXZkNGxyMnl6In0.TazuYLajm4MWY3PeTJrlOQ'; 
 
if ('geolocation' in navigator)  
{ 
  /* geolocation is available */ 
    navigator.geolocation.getCurrentPosition(success, fail) 
} else { 
  /* geolocation IS NOT available */ 
    fail(); 
} 
 
function success(position) 
{ 
    let caulfield = [position.coords.longitude, position.coords.latitude]; 
     
    //display the map 
    map = new mapboxgl.Map({ 
        container: 'map', 
        style: 'mapbox://styles/mapbox/streets-v11', //style URL 
        zoom: 9, //starting zoom 
        center: caulfield //starting position [lng,lat] 
    }); 
     
    bounds(); 
} 
 
function bounds(){ 
    let output = map.getBounds(); 
     
    let latlng = `${output._ne.lat},${output._ne.lng},${output._sw.lat},${output._sw.lng}` 
     
    let QueryString = "https://api.waqi.info/map/bounds/?token=:b103db3ad9de2b5c41fb0c09f71455e523f84fb5&latlng="+ latlng +"&callback=:display" 
     
    let api = document.createElement('api'); 
    api.src = QueryString; 
    document.body.appendChild(api); 
} 
 
function display(result) 
{ 
    console.log(result.data[0]) 
     
    for(let i=0; i<result.data.length; i++){ 
         
        let marker = new mapboxgl.Marker({ 
        color: AQItoColour(result.data[i].aqi) 
        }).setLngLat([result.data[i].lon, result.data[i].lat]) 
        .addTo(map); 
         
        new mapboxgl.Popup({offset: 30}) 
        .setLngLat([result.data[i].lon, result.data[i].lat]) 
        .setHTML("<h5>"+ result.data[i].station.name +"</h5>") 
        .addTo(map); 
         
    } 
     
    map.on('zoomend',function(){ 
        // run some code if the zoom changes 
        bounds(); 
    }); 
     
} 
 
function AQItoColour(aqi) 
{ 
    let colour; 
     
    if(aqi > 0 && aqi < 50){ 
        colour = "green"; 
    }else if(aqi > 51 && aqi < 100){ 
        colour = "yellow"; 
    }else if(aqi > 101 && aqi < 150){ 
        colour = "orange"; 
    }else if(aqi > 151 && aqi < 200){ 
        colour = "red"; 
    }else if(aqi > 201 && aqi < 300){ 
        colour = "purple"; 
    }else if(aqi > 300 && aqi < 500){ 
        colour = "maroon"; 
    }else 
        colour = "white" 
} 
 
function fail() 
{ 
    alert("Geolocation not available.") 
} 
