/* Write your function for implementing the Serial Search algorithm here */

/* Actual: 2.50ms,3.30ms,3.20ms,2.80ms,3.90ms,3.10ms,2.10ms,4.40ms,3.10ms,3.10ms
   Average: 3.15ms
*/

function serialSearchTest(data){
    let count = 0;
    while(count<data.length){
    if(data[count].emergency===true){
        return data[count].address;
    }else{
        count+=1;

    }
    }
    return null
}