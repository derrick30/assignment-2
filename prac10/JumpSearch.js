/* Write your function for implementing the Jump Search algorithm here */

/* Actual: 5.60ms,3.00ms,4.10ms,1.80ms,4.40ms,4.20ms,4.30ms,11.50ms,3.20ms,4.40ms
   Average: 4.65ms
*/

function jumpSearchTest(data)
{
    // Initialise the variables starting index and starting point to zero.
    let count = 0;
    let startingPoint = 0;
    // Pick a block size to jump over(eg: 4).
    let blockSize = 4;
    // Pick the object of the array at current index and check the emergency property.
    while(startingPoint < blockSize){
        if(data[count].emergency){
            return data[count].address;
        }else{
            count += blockSize;              
             if(count >= data.length){
                startingPoint += 1
                if(startingPoint < blockSize){
                    count=startingPoint;
                    
                }
            }
        
        }
    
    }
    return null;
    
}