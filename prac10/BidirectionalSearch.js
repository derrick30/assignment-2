/* Write your function for implementing the Bi-directional Search algorithm here */

/*  Actual: 2.00ms,2.10ms,1.60ms,1.00ms,2.20ms,2.60ms,2.50ms,5.70ms,1.90ms,1.60ms
    Average: 2.32ms
*/
    
function bidirectionalSearchTest(data)
{
    let num = 0 
    let middle = data.length/2
    
    
    while ( num < middle ){
    if (data[middle - num].emergency === true){
        return data[middle - num].address
    }
    
    else if(data[middle + num].emergency === true){
        return data[middle + num].address
    }
    
    else {
        
        num += 1
        
        if ( num <= middle){
            
                             if (data[middle - num].emergency === true){
                             return data[middle - num].address
                             }

                             else if(data[middle + num].emergency === true){
                             return data[middle + num].address
                             }
            
                           }
        
        else if ( num >= middle){
                                return null
                                }
        }
    }
}