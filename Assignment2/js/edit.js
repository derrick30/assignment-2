let delete = document.getElementById("deleteLocation");
function delete()
{
    //Enables user to delete the added location
                    
}

let up = document.getElementById("moveUp");
function dragUp()
{
    //Enables user to change the order of the locations
                    
}

let down = document.getElementById("moveDown");
function dragDown()
{
    //Enables user to change the order of the locations
                   
}

function submit()
{
    obj = updateLSData("myKey")
                    
    //Redirect to schedule page
    window.location = "schedule.html"
}

function clear()
{
    //Clear all the addded stops
                    
    //Redirect to previous page
    window.location = "POI.html"
}
                
function retrieveLSData(key)
{
    let data = localStorage.getItem(key);
    try
    {
        data = JSON.parse(data);
    }
    catch(err){}
    finally
    {
        return data;
    }
}
                
function updateLSData(key, data)
{
    let json = JSON.stringify(data);
    localStorage.setItem(key, json);
}
