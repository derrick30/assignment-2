/*
*Team Name: B23
* Team member: Derrick Ng Jean Zack; Kong Kher Ying; Xinyue Gu
* ID: 31870503; 32574045; 31179339
* Last Modified: 16/1/2022
* Description: MCD4290 Engineering Mobile Apps - Assignment 2
*/
function journey() 
{    
    let yourJourney = ""; 
     
    for(let i=0; i<obj.location.length; i++) 
    { 
        yourJourney += `${obj.location[i].name}<br><center>--><center><br>` 
    } 
     
    console.log(yourJourney) 
    document.getElementById("journey").innerHTML = yourJourney 
} 
         
function retrieveLSData(key) 
{ 
    let data = localStorage.getItem(key); 
    try 
    { 
        data = JSON.parse(data); 
    } 
    catch(err){} 
    finally 
    { 
        return data; 
    } 
} 
                         
let obj = retrieveLSData("myKey") 
console.log(obj) 
                               
document.getElementById("start").innerHTML = obj.startDate 
document.getElementById("end").innerHTML = obj.endDate 
document.getElementById("children").innerHTML = obj.child 
document.getElementById("adults").innerHTML = obj.adult 
document.getElementById("locationS").innerHTML = obj.location[0].name 
document.getElementById("locationE").innerHTML = obj.location[obj.location.length-1].name 
              
journey() 
 
function showSummary() 
{ 
    //Redirect to index.html - Summary Page 
    window.location = "index.html" 
} 
    