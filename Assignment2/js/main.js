/*
*Team Name: B23
* Team member: Derrick Ng Jean Zack; Kong Kher Ying; Xinyue Gu
* ID: 31870503; 32574045; 31179339
* Last Modified: 16/1/2022
* Description: MCD4290 Engineering Mobile Apps - Assignment 2
*/
function updateLSData(key, data)
{
    let json = JSON.stringify(data);
    localStorage.setItem(key, json);
}
                
function startMyTrip()
{
    let newVacation = {

    startDate: document.getElementById("start").value,
    endDate: document.getElementById("end").value,
    child: document.getElementById("children").value,
    adult: document.getElementById("adults").value,
    car: document.getElementById("car").value,
    distance : parseInt(document.getElementById("car").value),
    location: []
    
    }
    
    updateLSData("myKey",newVacation)

    //redirect to index html 
    window.location = 'POI.html'
    
}
