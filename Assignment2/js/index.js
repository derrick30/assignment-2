/*
*Team Name: B23
* Team member: Derrick Ng Jean Zack; Kong Kher Ying; Xinyue Gu
* ID: 31870503; 32574045; 31179339
* Last Modified: 16/1/2022
* Description: MCD4290 Engineering Mobile Apps - Assignment 2
*/
let wayPointsArray = []
let currentLocation ={
    name:"",
    lng:"",
    lat:"",
    label: ""
}
let vacation = retrieveLSData('myKey')

mapboxgl.accessToken = 'pk.eyJ1Ijoia2toZXIyOCIsImEiOiJja3g1d2JscmYxY2tvMnVwbXZkNGxyMnl6In0.TazuYLajm4MWY3PeTJrlOQ';
if ('geolocation' in navigator)  
{ 
    /* geolocation is available */ 
    navigator.geolocation.getCurrentPosition(success, fail)
} else { 
    /* geolocation IS NOT available */ 
    fail(); 
}
function success(position) 
{ 
    currentLocation.lng =position.coords.longitude
    currentLocation.lat = position.coords.latitude
    sendWebServiceRequestForReverseGeocoding(currentLocation.lat,currentLocation.lng,'reversecallback')
    //display the map 
    map = new mapboxgl.Map({ 
        container: 'map', 
        style: 'mapbox://styles/mapbox/streets-v11', //style URL 
        zoom: 9, //starting zoom 
        center: [currentLocation.lng,currentLocation.lat] //starting position [lng,lat] 
    }); 
    bounds(); 
} 
function reversecallback(result){
    currentLocation.label = result.results[0].formatted
}
function bounds()
{ 
    let output = map.getBounds(); 
    
    let latlng = `${output._ne.lat},${output._ne.lng},${output._sw.lat},${output._sw.lng}` 
    
    let QueryString = "https://api.waqi.info/map/bounds/?token=:b103db3ad9de2b5c41fb0c09f71455e523f84fb5&latlng="+ latlng +"&callback=:display" 
    
    let api = document.createElement('api'); 
    
    api.src = QueryString; 
    document.body.appendChild(api); 
} 
function fail(){ 
    alert("Geolocation not available.") 
}
let totalDistance = 0;

function drawlines(results)
{
    totalDistance += results.routes[0].distance
    for (let i = 0; i < results.routes[0].geometry.coordinates.length; i++){
        wayPointsArray.push(results.routes[0].geometry.coordinates[i])
    }
    document.getElementById('distance').innerHTML = (totalDistance/1000).toFixed(2) + 'km'
}
// Code added here will run when the page loads.
function panToCurrentLocation()
{ 
    // Code added here will run when the "Pan to Current Location" button is clicked.
    map.panTo([currentLocation.lng,currentLocation.lat])
    
    let popup = new mapboxgl.Popup({offset: 30})
    .setLngLat([currentLocation.lng,currentLocation.lat])
    .setHTML(currentLocation.label)
    .addTo(map);
    
    let marker = new mapboxgl.Marker()
    .setLngLat([currentLocation.lng,currentLocation.lat])
    .addTo(map);
    
    for(let i=0; i<vacation.location.length; i++){
        let popup = new mapboxgl.Popup({offset: 30})
        .setLngLat([vacation.location[i].lng,vacation.location[i].lat])
        .setHTML(vacation.location[i].label)
        .addTo(map);
        
        let marker = new mapboxgl.Marker()
        .setLngLat([vacation.location[i].lng,vacation.location[i].lat])
        .addTo(map);
    } 
    
    if(wayPointsArray = []){
        for(let i = 0 ; i < vacation.location.length-1 ; i++)
        { 
            sendXMLRequestForRoute(vacation.location[i].lat,vacation.location[i].lng,vacation.location[i+1].lat,vacation.location[i+1].lng,drawlines)}
    }
}
function showPath()
{
    console.log(wayPointsArray)
    map.removeLayer('outline')
    removeLayerWithId('index')
    // Code added here will run when the "Show Path" button is clicked.
    map.addSource('route', {
        'type' : 'geojson',
        'data' : {
            'type' : 'Feature',
            'geometry' : {
                'type' : 'LineString',
                'coordinates' : wayPointsArray
            }
        }
    })
    
    map.addLayer({
        'id' : 'route',
        'type' : 'line',
        'source' : 'route',
        'layout' : {
            'line-join' : 'round',
            'line-cap' : 'round'
        },
        
        'paint': {
            'line-color' : '#888',
            'line-width' : 5
        }
    })
}

function showPolygon()
{
    removeLayerWithId('route')
    // Code added here will run when the "Show Polygon" button is clicked.
    // Add a data source containing GeoJSON data.
    map.addSource('index', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'geometry': {
                'type': 'Polygon',
                
                // These coordinates outline the locataion.
                'coordinates': [wayPointsArray,wayPointsArray[0]]
            }
        }
    });
    
    // Add a new layer to visualize the polygon.
    map.addLayer({
        'id': 'index',
        'type': 'fill',
        'source': 'index', // reference the data source
        'layout': {},
        'paint': {
            'fill-color': '#0080ff', // blue color fill
            'fill-opacity': 0.5
        }
    });
    // Add a black outline around the polygon.
    map.addLayer({
        'id': 'outline',
        'type': 'line',
        'source': 'index',
        'layout': {},
        'paint': {
            'line-color': '#000',
            'line-width': 3
        }
    });
}

// This function checks whether there is a map layer with id matching 
// idToRemove. If there is, it is removed.
function removeLayerWithId(idToRemove){
    let hasPoly = map.getLayer(idToRemove)
    if (hasPoly !== undefined)
    {
        map.removeLayer(idToRemove)
        map.removeSource(idToRemove)
    }
}


function retrieveLSData(key)
{
    let data = localStorage.getItem(key);
    try
    {
        data = JSON.parse(data);
    }
    catch(err){}
    finally
    {
        return data;
    }
}

let obj = retrieveLSData("myKey")
console.log(obj)

document.getElementById("start").innerHTML = obj.startDate
document.getElementById("end").innerHTML = obj.endDate
document.getElementById("children").innerHTML = obj.child
document.getElementById("adults").innerHTML = obj.adult
document.getElementById("car").innerHTML = obj.car
document.getElementById("locationS").innerHTML = obj.location[0].name
document.getElementById("locationE").innerHTML = obj.location[obj.location.length-1].name
                          